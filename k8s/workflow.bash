curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash



k config get-contexts 
k config use-context prod
k create ns flightfactor-dev
kubectl config set-context --current --namespace flightfactor-dev

kubectl create secret generic regcred \
    --from-file=.dockerconfigjson=/home/juno/.docker/config.json \
    --type=kubernetes.io/dockerconfigjson


helm create staticserverhelm

helm install staticserverhelm --dry-run --debug staticserverhelm
helm install staticserverhelm k8s/staticserverhelm
helm upgrade staticserverhelm k8s/staticserverhelm
helm uninstall staticserverhelm

k apply -f extra/certt-flightfactor-site.yaml
k get event
k apply -f extra/ingress.yaml

curl https://flightfactor.site

kubectl port-forward static-server-docker-868c5b99b9-v2tgr 8000:8000


#TEMPLATE
k get all
helm install templatehelm --dry-run --debug templatehelm
k apply -f k8s/extra/tempalteingress.yaml

helm install templatehelm k8s/templatehelm --dry-run --debug
helm install templatehelm k8s/templatehelm
helm upgrade templatehelm k8s/templatehelm

kubectl port-forward static-template-7b945f5fb4-njf27  8000:8000

