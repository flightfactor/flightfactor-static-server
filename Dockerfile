# syntax=docker/dockerfile:1
FROM --platform=$BUILDPLATFORM golang:alpine as builder
# FROM arm64v8/golang as builder
WORKDIR /src

RUN apk add -U tzdata
RUN apk --update add ca-certificates

COPY . .
# RUN apk add git
# Run go mod
RUN go mod download
RUN go mod verify

ARG TARGETOS TARGETARCH
RUN --mount=target=. \
    --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    GOOS=$TARGETOS GOARCH=$TARGETARCH CGO_ENABLED=0 go build -o /server ./cmd/k8staticservergolang

FROM scratch

COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /src/dist  /dist
COPY --from=builder /server .
# Expose ports
EXPOSE 8000

ENTRYPOINT ["/server"]