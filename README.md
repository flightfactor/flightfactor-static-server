# k8staticservergolang
## DOCS
https://betterprogramming.pub/path-to-a-perfect-go-dockerfile-f7fe54b5c78c  
https://medium.com/@pavelfokin/how-to-build-a-minimal-golang-docker-image-b4a1e51b03c8

# WORKFLOW (cross compilling)

go run ./cmd/k8staticservergolang  
curl localhost:8090  
GOOS=linux GOARCH=arm64 CGO_ENABLED=0 go build ./cmd/k8staticservergolang

docker buildx use $(docker buildx create --platform linux/amd64,linux/arm64)

docker buildx create --name mybuilde  
docker buildx create --use --name mybuild node-amd64  
docker buildx create --append --name mybuild node-arm64  
docker buildx use mybuilde   
docker buildx rm --all-inactive --force  

docker buildx build -t "remotejob/k8staticservergolang:latest" --platform linux/amd64,linux/arm64 --push . && kubectl rollout restart deployment $DEPLOYMENT -n $NAMESPACE

docker run -t -p 8090:8090 remotejob/k8staticservergolang